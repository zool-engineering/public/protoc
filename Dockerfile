FROM debian:10.5-slim

ENV GO_VERSION=1.14.9 \
    GRPC_VERSION=1.16.0 \
    PROTOBUF_VERSION=3.13.0 \
    PROTOC_GEN_GO=1.4.2 \
    PROTOC_GEN_GO_GRPC=1.0 \
    PROTOC_GEN_GRPC_GATEWAY_VERSION=2.0.0-beta.4

RUN apt-get update
RUN apt-get install -y --no-install-recommends wget git tar unzip ca-certificates && rm -rf /var/lib/apt/lists/*

# Download and unpack protobuf
RUN wget -nv "https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOBUF_VERSION}/protoc-${PROTOBUF_VERSION}-linux-x86_64.zip"
RUN unzip "protoc-${PROTOBUF_VERSION}-linux-x86_64.zip" -d "/usr/local"

RUN wget -nv "https://golang.org/dl/go${GO_VERSION}.linux-amd64.tar.gz"
RUN tar -C /usr/local -xzf go${GO_VERSION}.linux-amd64.tar.gz
RUN rm go${GO_VERSION}.linux-amd64.tar.gz

ENV GOPATH /go
ENV PATH /usr/local/go/bin:$GOPATH/bin:$PATH
RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"

RUN env GO111MODULE=on go get github.com/golang/protobuf/protoc-gen-go@v${PROTOC_GEN_GO} \
        google.golang.org/grpc/cmd/protoc-gen-go-grpc@v${PROTOC_GEN_GO_GRPC} \
        github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway@v${PROTOC_GEN_GRPC_GATEWAY_VERSION} \
        github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2@v${PROTOC_GEN_GRPC_GATEWAY_VERSION}

ENTRYPOINT ["protoc"]
